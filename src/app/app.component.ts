import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { MapsAPILoader, MouseEvent } from '@agm/core';
 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})



export class AppComponent implements OnInit {
  title: string = 'AGM project';
  latitude: number;
  longitude: number;
  zoom: number;
  address: string;
  location_formatted: any;
  private geoCoder;
 
  @ViewChild('search', {static:  true })
  public searchElementRef: ElementRef;
 
 
  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) { }
  

  getFormattedAddress(place) {
    //@params: place - Google Autocomplete place object
    //@returns: location_obj - An address object in human readable format
		let location_obj = {};
		for (let i in place.address_components) {
		  let item = place.address_components[i];
		  
		  location_obj['formatted_address'] = place.formatted_address;
		  if(item['types'].indexOf("locality") > -1) {
			location_obj['suburb'] = item['long_name']
		  } else if (item['types'].indexOf("administrative_area_level_1") > -1) {
			location_obj['province'] = item['short_name']
		  } else if (item['types'].indexOf("administrative_area_level_2") > -1) {
			location_obj['city'] = item['short_name']
		  } else if (item['types'].indexOf("street_number") > -1) {
			location_obj['street_number'] = item['short_name']
		  } else if (item['types'].indexOf("route") > -1) {
			location_obj['street_name'] = item['long_name']
		  } else if (item['types'].indexOf("country") > -1) {
			location_obj['country'] = item['long_name']
		  } else if (item['types'].indexOf("postal_code") > -1) {
			location_obj['postal_code'] = item['short_name']
		  }
		 
		}
		return location_obj;
  }

 
  ngOnInit() {
    //load Places Autocomplete - and get current location
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

 
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
 
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.location_formatted = this.getFormattedAddress(place);
          this.location_formatted["latitude"] = this.latitude;
          this.location_formatted["longitude"] = this.longitude;

          console.log(this.location_formatted);


            //this.zoom = 12;
        });
      });
    });
  }
 
  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
        this.getAddress(this.latitude, this.longitude);
        //this.location_obj = this.getFormattedAddress(this.getAddress(this.latitude, this.longitude));
        //console.log("Location Object: " + this.location_obj);
        //console.log(this.getFormattedAddress(google.maps.places));

      });
    }
  }
 
 
  markerDragEnd($event: MouseEvent) {
    console.log($event);
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
    //this.location_obj = this.getFormattedAddress(this.getAddress(this.latitude, this.longitude));
    //console.log("Location Object: " + this.location_obj);
    
  }
 
  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      //console.log(results[0].address_components); // Comment this out
      this.location_formatted = this.getFormattedAddress(results[0]);
      this.location_formatted["latitude"] = this.latitude;
      this.location_formatted["longitude"] = this.longitude;
      console.log(this.location_formatted);

      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;

        } else {
          //window.alert('No results found');
        }
      } else {
        //window.alert('Geocoder failed due to: ' + status);
      }
 
    });
  }
 
}